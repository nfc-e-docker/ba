'use strict';

const fs = require('fs');
const { certs } = require('../lib');
const { parseString } = require('xml2js');
const { should, expect } = require('chai');
const createClient = require('../lib/client');

let client = null;
const invoice = fs.readFileSync(`${__dirname}/xml/unsigned.xml`, { encoding: 'utf-8' });
let obj = null;
describe('Microservice BA', () => {
  before(done => {
    createClient(process.env.GRPC_HOST, process.env.GRPC_PORT, 'InvoiceService')
      .then(_client => {
        client = _client;
        parseString(
          invoice,
          { explicitArray: false, explicitRoot: true, normalizeTags: false, attrkey: 'attrkey' },
          (err, json) => {
            should().not.exist(err);
            obj = json;
            done();
          }
        );
      })
      .catch(err => {
        done(err);
      });
    // Xml to JSON parser
  });

  it('Method: echo', done => {
    client.echo({ ping: 'ping' }, (err, res) => {
      should().not.exist(err);
      should().exist(res);
      expect(res.pong).to.be.equal('pong');
      done();
    });
  });
  it('Method: status', done => {
    client.status({}, (err, res) => {
      should().not.exist(err);
      expect(res.status).to.be.an('Boolean');
      done();
    });
  });
  it('Method: process', done => {
    client.process(
      { invoice: JSON.stringify(obj), key: certs.keyCertPairs[0].private_key, crt: certs.keyCertPairs[0].cert_chain },
      (err, res) => {
        should().not.exist(err);
        expect(res.invoice).to.be.a('string');
        // should().exist(JSON.parse(res.invoice).enviNFe.protNFe);
        done();
      }
    );
  });
  it('Method: cancel', done => {
    const key = obj.enviNFe.NFe.infNFe.attrkey.Id.replace('NFe', '');
    client.cancel({ key }, (err, res) => {
      should().not.exist(err);
      expect(res.status).to.be.equal(true);
      done();
    });
  });
});
