'use strict';

const server = require('../server');

describe('Module: BA', () => {
  before(done => {
    // Starts the server.
    server
      .init()
      .then(() => {
        done();
      })
      .catch(err => {
        done(err);
      });
  });
  // BA tests.
  require('./ba');
});
