# PLATAFORMA NFC-E: UMA ARQUITETURA ORIENTADA À MICROSSERVIÇOS

Mirosserviço de emissão de NFC-e do estado da Bahia.

![alt text](./ba.jpg "Mirosserviço de NFC-e do estado da Bahia")

## Conceito de NFC-e

A Nota Fiscal de Consumidor Eletrônica - NFC-e consiste em um documento eletrônico que tem como objetivo documentar uma operação de circulação de mercadorias ou prestação de serviços, no campo de incidência do ICMS - Imposto sobre Operações relativas à Circulação de Mercadorias e Prestação de Serviços de Transporte Interestadual e Intermunicipal e de Comunicação.

- Instalação de dependências

  - `npm install`

- Testes de unidade

  - `npm run test`

- Inicialização

  - `npm start`
