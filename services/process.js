/**
 * Invoice processing service
 */

'use strict';

require('dotenv').config();
const uuid = require('uuid/v4');
const crypto = require('crypto');
const { Builder } = require('xml2js');
const createClient = require('../lib/client');
const { mongo, registry, askGateway, promisify } = require('../lib');

module.exports = (invoice, key, crt, callback) => {
  // Simulates a successful invoice authorization process by inserting port NFe tag inside original xml.

  // Buffer to string.
  key = key.toString('utf-8');
  crt = crt.toString('utf-8');
  const obj = JSON.parse(invoice);
  const invoiceKey = obj.enviNFe.NFe.infNFe.attrkey.Id.replace('NFe', '');
  const protocolNumber = uuid();

  if (process.env.TEST_TYPE === 'integrated') {
    const service = registry.find('signature');
    if (!service) {
      askGateway('signature', async (err, res) => {
        if (err) return callback(err);

        // Client creation and promisify.
        const client = await createClient(res.ipv4, res.port, 'SignatureService');
        promisify(client);

        const builder = new Builder({ headless: true, explicitRoot: false, renderOpts: { pretty: false } });
        const xml = builder.buildObject(obj);
        // Calls signature service.
        await client.signature({ xml, key, crt });

        // Signs the invoice and send to the government (for simplification, here we'll just add 'protNFe' tag that simulates an authorization).
        // "res" represents the signed xml, and it would be sent to the government infrastructure.
        obj.enviNFe.protNFe = {
          attrkey: { xmlns: 'http://www.portalfiscal.inf.br/nfe', versao: '4.00' },
          infProt: {
            attrkey: { Id: `ID${protocolNumber}` },
            tpAmb: 2,
            verAplic: 'SVAN.PrLot_4.2.3',
            chNFe: invoiceKey,
            dhRecbto: new Date(),
            nProt: protocolNumber,
            digVal: crypto.randomBytes(19).toString('base64'),
            cStat: 100,
            xMotivo: 'Autorizado o uso da NF-e'
          }
        };
        // Saves invoice in the MongoDB.
        mongo.insert({ key: invoiceKey, obj }).then(() => {
          callback(null, JSON.stringify(obj));
        });
      });
    }
  } else {
    mongo.insert({ key: invoiceKey, obj }).then(() => {
      callback(null, JSON.stringify(obj));
    });
  }
};
