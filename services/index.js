'use strict';

module.exports = {
  cancelService: require('./cancel'),
  statusService: require('./status'),
  documentService: require('./document'),
  processService: require('./process')
};
