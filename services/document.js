'use strict';

const createClient = require('../lib/client');
const endpoints = require('../static/endpoints');
const { askGateway, promisify, promiseHandler } = require('../lib');

let data = null;
let err = null;

module.exports = (args, callback) => {
  const { invoice, cic, csc, width } = args;
  askGateway('danfe', async (_err, res) => {
    if (_err) return callback(_err);
    // Client creation and promisify.
    [err, data] = await promiseHandler(createClient(res.ipv4, res.port, 'InvoiceService'));
    if (err) return callback(err);
    const client = data;
    promisify(client);
    // I need to change that. It should be defined by invoice content (homologation or production environment).
    const host = endpoints.hologation.NFeConsultaQRCode;
    // Retrieves  the DANFE as pdf.
    [err, data] = await promiseHandler(client.document({ invoice, cic, csc, width, host }));
    if (err) return callback(err);
    callback(null, data.pdf);
  });
};
