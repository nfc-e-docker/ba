'use strict';

module.exports = Object.freeze({
  production: {
    NFeQRCode: 'https://nfce.fazenda.mg.gov.br/portalnfce/sistema/qrcode.xhtml',
    NFeConsultaQRCode: 'http://nfce.fazenda.mg.gov.br/portalnfce',
    NFeAutorizacao: 'https://nfce.fazenda.mg.gov.br/nfce/services/NFeAutorizacao4?wsdl',
    NFeRetAutorizacao: 'https://nfce.fazenda.mg.gov.br/nfce/services/NFeRetAutorizacao4?wsdl',
    NfeInutilizacao: 'https://nfce.fazenda.mg.gov.br/nfce/services/NFeInutilizacao4?wsdl',
    NfeConsultaProtocolo: 'https://nfce.fazenda.mg.gov.br/nfce/services/NFeConsultaProtocolo4?wsdl',
    NfeStatusServico: 'https://nfce.fazenda.mg.gov.br/nfce/services/NFeStatusServico4?wsdl',
    RecepcaoEvento: 'https://nfce.fazenda.mg.gov.br/nfce/services/NFeRecepcaoEvento4?wsdl'
  },
  hologation: {
    NFeQRCode: 'https://nfce.fazenda.mg.gov.br/portalnfce/sistema/qrcode.xhtml',
    NFeConsultaQRCode: 'http://hnfce.fazenda.mg.gov.br/portalnfce/',
    NFeAutorizacao: 'https://hnfce.fazenda.mg.gov.br/nfce/services/NFeAutorizacao4?wsdl',
    NFeRetAutorizacao: 'https://hnfce.fazenda.mg.gov.br/nfce/services/NFeRetAutorizacao4?wsdl',
    NfeInutilizacao: 'https://hnfce.fazenda.mg.gov.br/nfce/services/NFeInutilizacao4?wsdl',
    NfeConsultaProtocolo: 'https://hnfce.fazenda.mg.gov.br/nfce/services/NFeConsultaProtocolo4?wsdl',
    NfeStatusServico: 'https://hnfce.fazenda.mg.gov.br/nfce/services/NFeStatusServico4?wsdl',
    RecepcaoEvento: 'https://hnfce.fazenda.mg.gov.br/nfce/services/NFeRecepcaoEvento4?wsdl'
  }
});
