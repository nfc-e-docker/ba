'use strict';

/**
 * @description global error handler (database errors)
 * @param {string} promise - A promise to be resolved.
 * @returns {Promise} - Returns the Promise object that is resolved with the given value.
 */
module.exports = promise => {
  return promise.then(data => [null, data]).catch(err => [err]);
};
