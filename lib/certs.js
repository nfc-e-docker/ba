'use strict';

const fs = require('fs');
const path = require('path');

module.exports = {
  // This is necessary only if the client uses the self-signed certificate.
  rootCerts: fs.readFileSync(path.join(__dirname, '..', 'certs', 'ca.crt')),
  keyCertPairs: [
    {
      cert_chain: fs.readFileSync(path.join(__dirname, '..', 'certs', 'server.crt')),
      private_key: fs.readFileSync(path.join(__dirname, '..', 'certs', 'server.key'))
    }
  ]
};
