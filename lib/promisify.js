'use strict';
/**
 * promisify
 * @param client grpc client
 */
function promisify(client) {
  Object.keys(Object.getPrototypeOf(client)).forEach(functionName => {
    const originalFunction = client[functionName];

    client[functionName] = (request, callback) => {
      if (callback && typeof callback === 'function') {
        return originalFunction.call(client, request, (err, res) => {
          callback(err, res);
        });
      }

      return new Promise((resolve, reject) => {
        originalFunction.call(client, request, (err, res) => {
          if (err) reject(err);
          else resolve(res);
        });
      });
    };
  });
}

module.exports = promisify;
