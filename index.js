'use strict';

const server = require('./server');

server
  .init()
  .then(() => {
    // eslint-disable-next-line no-console
    console.info(`\tBA service is running on ${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`);
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error(err);
    process.exit(1);
  });
